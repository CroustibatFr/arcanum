import glv
from arcanum_data import ArcanumData

class Stressor(ArcanumData):
  """ The Stressor class """
  def __init__(self, json_data):
    super().__init__(json_data)
