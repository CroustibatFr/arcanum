
import json
import os
from pprint import pprint
import glv
import wiki_renderer
from glv import devmsg
from class_class import Class
from dungeon import Dungeon
from enchant import Enchant
from encounter import Encounter
from event import Event
from furniture import Furniture
from home import Home
from locale_class import Locale
from material import Material
from monster import Monster
from player import Player
from potion import Potion
from rare import Rare
from reagent import Reagent
from resource import Resource
from skill import Skill
from spell import Spell
from stressor import Stressor
from task import Task
from upgrade import Upgrade
from weapon import Weapon
from tags import Tags

class AllData:
  """
  self class holds the accumulated game data read from the .json files.
  It also holds the code to make category pages such as 'Locales' and 'Dungeons'
  on the wiki
  """
  all_data = {
    "classes": {},
    "dungeons": {},
    "enchants": {},
    "encounters": {},
    "events": {},
    "furnitures": {},
    "homes": {},
    "locales": {},
    "materials": {},
    "monsters": {},
    "player": {},
    "potions": {},
    "rares": {},
    "reagents": {},
    "resources": {},
    "skills": {},
    "spells": {},
    "stressors": {},
    "tasks": {},
    "upgrades": {},
    "weapons": {},
    "tags": {}
  }
  id_slug = {}


  @staticmethod
  def generate_pages():
    glv.devmsg("Generating Pages") if glv.args.verbose > 0 else None
    # pp = pprint.PrettyPrinter(indent=4, sort_dicts=False)
    # pp.pprint(vars(Data))
    Class.category_page()
    Dungeon.category_page()
    Enchant.category_page()
    Encounter.category_page()
    Furniture.category_page()
    Home.category_page()
    Locale.category_page()
    Monster.category_page()
    Player.category_page()
    Potion.category_page()
    Tags.category_page()
    if not glv.args.main:
      Class.entry_pages()
      Encounter.entry_pages()
      #Furniture.entry_pages()
      #Home.entry_pages()
      Locale.entry_pages()
      Player.entry_pages()
      Potion.entry_pages()

    # UNFINISHED BELOW HERE #
    # Event needs some discussion
    #Event.category_page()
    
    #Material.category_page()
    #Rare.category_page()
    #Reagent.category_page()
    #Resource.category_page()
    #Resource.entry_pages()
    #Skill.category_page()
    #Spell.category_page()
    #Stressor.category_page()
    #Task.category_page()
    #Task.entry_pages()
    #Upgrade.category_page()
    #Weapon.category_page()
  
  @staticmethod
  def get_object(object_id):
    """
    Provided a valid object id, return the object
    :param object_id: id string
    :return: object, or None if not found
    """
    if object_id in glv.data.all_data["classes"]:
      return glv.data.all_data["classes"][object_id]
    elif object_id in glv.data.all_data["dungeons"]:
      return glv.data.all_data["dungeons"][object_id]
    elif object_id in glv.data.all_data["enchants"]:
      return glv.data.all_data["enchants"][object_id]
    elif object_id in glv.data.all_data["encounters"]:
      return glv.data.all_data["encounters"][object_id]
    elif object_id in glv.data.all_data["events"]:
      return glv.data.all_data["events"][object_id]
    elif object_id in glv.data.all_data["furnitures"]:
      return glv.data.all_data["furnitures"][object_id]
    elif object_id in glv.data.all_data["homes"]:
      return glv.data.all_data["homes"][object_id]
    elif object_id in glv.data.all_data["locales"]:
      return glv.data.all_data["locales"][object_id]
    elif object_id in glv.data.all_data["materials"]:
      return glv.data.all_data["materials"][object_id]
    elif object_id in glv.data.all_data["monsters"]:
      return glv.data.all_data["monsters"][object_id]
    elif object_id in glv.data.all_data["player"]:
      return glv.data.all_data["player"][object_id]
    elif object_id in glv.data.all_data["potions"]:
      return glv.data.all_data["potions"][object_id]
    elif object_id in glv.data.all_data["rares"]:
      return glv.data.all_data["rares"][object_id]
    elif object_id in glv.data.all_data["reagents"]:
      return glv.data.all_data["reagents"][object_id]
    elif object_id in glv.data.all_data["resources"]:
      return glv.data.all_data["resources"][object_id]
    elif object_id in glv.data.all_data["skills"]:
      return glv.data.all_data["skills"][object_id]
    elif object_id in glv.data.all_data["spells"]:
      return glv.data.all_data["spells"][object_id]
    elif object_id in glv.data.all_data["stressors"]:
      return glv.data.all_data["stressors"][object_id]
    elif object_id in glv.data.all_data["tasks"]:
      return glv.data.all_data["tasks"][object_id]
    elif object_id in glv.data.all_data["upgrades"]:
      return glv.data.all_data["upgrades"][object_id]
    elif object_id in glv.data.all_data["weapons"]:
      return glv.data.all_data["weapons"][object_id]
    elif "player." in object_id:
      sub_id = object_id.replace("player.", "")
      return glv.data.get_object(sub_id)
    
    # If we didn't find it, return None
    # devmsg(f"Object ID '{object_id}' not found!")
    return None

  @classmethod
  def load_base_data(cls):
    """
    Load and process the base data files, storing data in class-level
    dictionaries
    """
    with open("../data/classes.json") as f:
      for item in json.load(f):
        cls.all_data["classes"][item["id"]] = Class(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Classes/", item["id"])
    with open("../data/dungeons.json") as f:
      for item in json.load(f):
        cls.all_data["dungeons"][item["id"]] = Dungeon(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Dungeons/", item["id"])
    with open("../data/enchants.json") as f:
      for item in json.load(f):
        cls.all_data["enchants"][item["id"]] = Enchant(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Enchants/", item["id"])
    with open("../data/encounters.json") as f:
      for item in json.load(f):
        cls.all_data["encounters"][item["id"]] = Encounter(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Encounters/", item["id"])
    with open("../data/events.json") as f:
      for item in json.load(f):
        cls.all_data["events"][item["id"]] = Event(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Events/", item["id"])
    with open("../data/furniture.json") as f:
      for item in json.load(f):
        cls.all_data["furnitures"][item["id"]] = Furniture(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Furnitures/", item["id"])
    with open("../data/homes.json") as f:
      for item in json.load(f):
        cls.all_data["homes"][item["id"]] = Home(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Homes/", item["id"])
    with open("../data/locales.json") as f:
      for item in json.load(f):
        cls.all_data["locales"][item["id"]] = Locale(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Locales/", item["id"])
    with open("../data/materials.json") as f:
      for item in json.load(f):
        cls.all_data["materials"][item["id"]] = Material(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Materials/", item["id"])
    with open("../data/monsters.json") as f:
      for item in json.load(f):
        cls.all_data["monsters"][item["id"]] = Monster(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Monsters/", item["id"])
    with open("../data/player.json") as f:
      for item in json.load(f):
        cls.all_data["player"][item["id"]] = Player(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Player/", item["id"])
    with open("../data/potions.json") as f:
      for item in json.load(f):
        cls.all_data["potions"][item["id"]] = Potion(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Potions/", item["id"])
    with open("../data/rares.json") as f:
      for item in json.load(f):
        cls.all_data["rares"][item["id"]] = Rare(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Rares/", item["id"])
    with open("../data/reagents.json") as f:
      for item in json.load(f):
        cls.all_data["reagents"][item["id"]] = Reagent(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Reagents/", item["id"])
    with open("../data/resources.json") as f:
      for item in json.load(f):
        cls.all_data["resources"][item["id"]] = Resource(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Resources/", item["id"])
    with open("../data/skills.json") as f:
      for item in json.load(f):
        cls.all_data["skills"][item["id"]] = Skill(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Skills/", item["id"])
    with open("../data/spells.json") as f:
      for item in json.load(f):
        cls.all_data["spells"][item["id"]] = Spell(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Spells/", item["id"])
    with open("../data/stressors.json") as f:
      for item in json.load(f):
        cls.all_data["stressors"][item["id"]] = Stressor(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Stressors/", item["id"])
    with open("../data/tasks.json") as f:
      for item in json.load(f):
        cls.all_data["tasks"][item["id"]] = Task(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Tasks/", item["id"])
    with open("../data/upgrades.json") as f:
      for item in json.load(f):
        cls.all_data["upgrades"][item["id"]] = Upgrade(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Upgrades/", item["id"])
    with open("../data/weapons.json") as f:
      for item in json.load(f):
        cls.all_data["weapons"][item["id"]] = Weapon(item)
        glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Weapons/", item["id"])

  @classmethod
  def load_module_data(cls):
    """
    Load and process the module files by adding them to the
    existing class-level dictionaries
    """
    directory = "../data/modules"
    for module_name in os.listdir(directory):
      module_path = os.path.join(directory, module_name)
      # glv.devmsg(f"Module name: {module_path}")
      if module_path == "../data/modules/halloween.json":
        continue # skip halloween too many broken bits
      with open(module_path) as f:
        module_data = json.load(f)
      # pp.pprint(module_data)
      sym = None
      if "sym" in module_data:
        sym = module_data["sym"]  # winter module, etc
      if "data" in module_data:
        mdata = module_data["data"]
        for key in mdata:
          if key == "classes":
            for item in mdata[key]:
              cls.all_data["classes"][item["id"]] = Class(item)
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Classes/", item["id"])
          elif key == "dungeons":
            for item in mdata[key]:
              cls.all_data["dungeons"][item["id"]] = Dungeon(item)
              cls.all_data["dungeons"][item["id"]].sym = sym
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Dungeons/", item["id"])
          elif key == "enchants":
            for item in mdata[key]:
              cls.all_data["enchants"][item["id"]] = Enchant(item)
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Enchants/", item["id"])
          elif key == "encounters":
            for item in mdata[key]:
              cls.all_data["encounters"][item["id"]] = Encounter(item)
              cls.all_data["encounters"][item["id"]].sym = sym
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Encounters/", item["id"])
          elif key == "events":
            for item in mdata[key]:
              cls.all_data["events"][item["id"]] = Event(item)
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Events/", item["id"])
          elif key == "furniture":
            for item in mdata[key]:
              cls.all_data["furnitures"][item["id"]] = Furniture(item)
              cls.all_data["furnitures"][item["id"]].sym = sym
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Furnitures/", item["id"])
          elif key == "homes":
            for item in mdata[key]:
              cls.all_data["homes"][item["id"]] = Home(item)
              cls.all_data["homes"][item["id"]].sym = sym
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Homes/", item["id"])
          elif key == "locales":
            for item in mdata[key]:
              cls.all_data["locales"][item["id"]] = Locale(item)
              cls.all_data["locales"][item["id"]].sym = sym
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Locales/", item["id"])
          elif key == "materials":
            for item in mdata[key]:
              cls.all_data["materials"][item["id"]] = Material(item)
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Materials/", item["id"])
          elif key == "monsters":
            for item in mdata[key]:
              cls.all_data["monsters"][item["id"]] = Monster(item)
              cls.all_data["monsters"][item["id"]].sym = sym
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Monsters/", item["id"])
          elif key == "potions":
            for item in mdata[key]:
              cls.all_data["potions"][item["id"]] = Potion(item)
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Potions/", item["id"])
          elif key == "rares":
            for item in mdata[key]:
              cls.all_data["rares"][item["id"]] = Rare(item)
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Rares/", item["id"])
          elif key == "resources":
            for item in mdata[key]:
              cls.all_data["resources"][item["id"]] = Resource(item)
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Resources/", item["id"])
          elif key == "skills":
            for item in mdata[key]:
              cls.all_data["skills"][item["id"]] = Skill(item)
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Skills/", item["id"])
          elif key == "spells":
            for item in mdata[key]:
              cls.all_data["spells"][item["id"]] = Spell(item)
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Spells/", item["id"])
          elif key == "tasks":
            for item in mdata[key]:
              cls.all_data["tasks"][item["id"]] = Task(item)
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Tasks/", item["id"])
          elif key == "upgrades":
            for item in mdata[key]:
              cls.all_data["upgrades"][item["id"]] = Upgrade(item)
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Upgrades/", item["id"])
          elif key == "weapons":
            for item in mdata[key]:
              cls.all_data["weapons"][item["id"]] = Weapon(item)
              glv.data.id_slug[item["id"]] =  glv.gen_slug_title("home/Weapons/", item["id"])
          elif key == "fupgrades":
            # Unimplemented by the game as of 2021-03-04
            pass
          elif glv.args.verbose > 0:
            glv.devmsg(f"Unknown module key '{key}'")
          
            #exit(0)

  def id_list_to_wiki(self, id_list, style="flow"):
    """
    Take a list of game IDs, and return a string of slugs separated by breaks
    :param id_list: list of game IDs
    :param style: 'single' for <br> separated lines, 'flow' for commas
    :return: string of break-separated GitLab markdown slugs
    """
    result_list = []
    for item in id_list:
      original_item = item
      suffix = []
      prefix = []
      if type(item) == list:
        prefix = [item[0]]
        item = item[1]
      # devmsg(f"prefix({prefix}) item({item})")
      if item.endswith(".mod.space.max"):
        suffix.insert(0, "Space Max")
        item = item.replace(".mod.space.max", "")
      if item == "inv.max":
        suffix.insert(0, "Space")
        item = "Inventory"
      if item.endswith(".max"):
        suffix.insert(0, "Max")
        item = item.replace(".max", "")
      if item == "element.rate":
        suffix.insert(0, "Rate")
        item = "Element"
      if item.endswith(".rate"):
        suffix.insert(0, "Rate")
        item = item.replace(".rate", "")
      if item.endswith(".keep"):
        suffix.insert(0, "Keep")
        item = item.replace(".keep", "")
      if item.endswith(".exp"):
        suffix.insert(0, "Exp")
        item = item.replace(".exp", "")
      if item == "a_travel":
        suffix.insert(0, "Task")
      # glv.devmsg(item, prefix, suffix, original_item)
      if item in self.id_slug:
        result_list.append(wiki_renderer.get_markdown_link(wiki_renderer.get_title_with_suffix(glv.data.id_slug[item][1], suffix=suffix), glv.data.id_slug[item][0], prefix=prefix))
      elif item.startswith("player."):
        sub_item = item.split(".")[1]
        if item in self.all_data["player"]:
          result_list.append(self.all_data["player"][sub_item].to_slug(prefix=prefix, suffix=suffix))
        else:
          result_list.append(sub_item)
      else:
        # No slugs here, we can't find the object..
        if not item.startswith("["):
          devmsg(original_item) if glv.args.verbose > 1 else None
          devmsg(f"ID '{item}' not found! HALP!!") if glv.args.verbose > 1 else None
        text = item.title()
        devmsg(f"text({text}) suffix({suffix}) prefix({prefix})") if glv.args.verbose > 1 else None
        if suffix:
          text += " " + " ".join(suffix)
        if prefix:
          for p in prefix:
            # text.insert(0, p)
            text += f": {p}"
        devmsg(f"text: {text}") if glv.args.verbose > 1 else None
        result_list.append(text)
    result_list.sort()
    sep = ", "
    if style == "single":
      sep = ",<br>"
    return sep.join(result_list)

  def small_print(self):
    for datatype in self.all_data:
      pprint("{0} {1}".format(datatype, len(self.all_data[datatype])))

  def big_print(self):
    for datatype in self.all_data:
      glv.devmsg(datatype)
      pprint(self.all_data[datatype])

  def __str__(self):
    applist = []
    string = ""
    for datatype in self.all_data:
      applist.append(datatype)
      for data in self.all_data[datatype]:
        applist.append("  {0}:".format(data))
        applist.append(str(self.all_data[datatype][data]))
    return "\n".join(applist)
