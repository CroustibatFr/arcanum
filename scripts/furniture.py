import glv
import wiki_renderer
from arcanum_data import ArcanumData

class Furniture(ArcanumData):
  """ The Furniture class """
  def __init__(self, json_data):
    super().__init__(json_data)

  @staticmethod
  def category_page():
    """ Generate -/wikis/Furniture """
    nm = "Name"
    ta = "Tags"
    bm = "Base<br>Maximum"
    co = "Cost"
    bo = "Bonus"
    re = "Requires"
    output = \
      f"|{nm}|{ta}|{bm}|{co}|{bo}|{re}|\n" \
      f"|:---|:---|:---|:---|:---|:---|\n"
    tmp = []
    for furnishing_id in glv.data.all_data["furnitures"].keys():
      furnishing = glv.data.all_data["furnitures"][furnishing_id]
      tmp.append(furnishing.category_line() + "\n")
    tmp.sort()
    output += "".join(tmp)
    title = "Furnitures"
    path = "home/"
    diff = wiki_renderer.save_page(title, path, output)
    glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
    glv.wikipost(title, path, output, diff)

  def category_line(self):
    """
    Transform this Furniture object into a line of Markdown text
    :return: string of gitlab markdown text
    """
    symbol = self.get_symbol()
    title = f"[{glv.data.id_slug[self.id][1]}]({glv.data.id_slug[self.id][0]})"
    if self.desc is not None and self.desc != "":
      title += f"<br>*{self.desc}*"
    tags = self.tags
    if tags is None:
      tags = ""
    else:
      if "," in tags:
        temp_tags = []
        for t in tags.split(','):
          if t.startswith("t_"):
            t = t.replace("t_", "")
          temp_tags.append(t)
        temp_tags.sort()
        tags = ", ".join(temp_tags)
      else:
        if tags.startswith("t_"):
          tags = tags.replace("t_", "")
      tags = f"*{tags}*"
    bmax = self.max
    if bmax is None:
      bmax = ""
    cost = glv.data.id_list_to_wiki(self.get_run_costs(), style="single")
    bonus = glv.data.id_list_to_wiki(self.get_mods(), style="single")
    require = self.get_requirements()
    return f"|{symbol} {title}|{tags}|{bmax}|{cost}|{bonus}|{require}|"
