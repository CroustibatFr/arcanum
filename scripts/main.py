import argparse
from pprint import pprint
import glv
import sys
from all_data import AllData

'''
Runs a 4 step process:
1) Parse args to determine what generation and upload settings to use.
2) Parse raw data
3) Render wiki page
4) Upload renders
'''

def main():
  if not glv.args.all:
    glv.devmsg("Removing duplicate pages.") if glv.args.verbose > 0 else None
    glv.args.pages = list(set(glv.args.pages))
    glv.devmsg(glv.args) if glv.args.verbose > 0 else None


  # Load raw data.
  glv.devmsg("Initializing data.") if glv.args.verbose > 0 else None
  glv.data = AllData()
  glv.devmsg("Loading base data.") if glv.args.verbose > 0 else None
  glv.data.load_base_data()
  glv.devmsg("Loading module data.") if glv.args.verbose > 0 else None
  glv.data.load_module_data()

  glv.data.small_print() if glv.args.verbose == 2 else None
  glv.data.big_print() if glv.args.verbose == 3 else None
  glv.devmsg(glv.data) if glv.args.verbose == 4 else None

  # Render and upload wiki pages
  glv.data.generate_pages()

def make_parser():
  parser = argparse.ArgumentParser(description="Main runner for the gitlab arcanum wikifier.")
  parser.add_argument("-u", "--upload", action="store_true", help="Upload generated files.")
  parser.add_argument("-m", "--main", action="store_true", help="Generates only main mages.")
  parser.add_argument("-t", "--test", action="store_true", help="Only has an affect if upload is true. Uploads only main pages, and uploads to test pages.")
  parser.add_argument("-d", "--diff", action="store_true", help="If upload is true, diffs existing local file and generated ones. Queues only files for upload that have differences.")
  parser.add_argument("-v", "--verbose", action="count", default=0, help="Enables more output for debugging purposes.")
  group = parser.add_mutually_exclusive_group()
  group.add_argument("-n", "--no_graph", action="store_true", default=True, help="Do not generate a class graph. This is the default.")
  group.add_argument("-g", "--graph", action="store_true", help="Generate a class graph.")
  group = parser.add_mutually_exclusive_group()
  group.add_argument("-a", "--all", action="store_true", help="Generate all pages.")
  group.add_argument("-p", "--pages", metavar="PAGE_OPTIONS", choices=glv.mains, nargs="+", help="NOT IMPLEMENTED. Which pages to generate. PAGE_OPTIONS={{{0}}}".format(', '.join(glv.mains)))
  return parser


if __name__ == '__main__':
  glv.mains = [
    "classes",
    "dungeons",
    "enchants",
    "encounters",
    "events",
    "furnitures",
    "homes",
    "locales",
    "materials",
    "monsters",
    "player",
    "potions",
    "rares",
    "reagents",
    "resources",
    "skills",
    "spells",
    "stressors",
    "tasks",
    "upgrades",
    "weapons"
    ]
  parser = make_parser()
  glv.args = parser.parse_args()

  if len(sys.argv) == 1:
    parser.print_help()
    exit(0)

  glv.devmsg(glv.args) if glv.args.verbose > 0 else None

  # Verify and parse args for valid generation parameters 
  if not glv.args.all and glv.args.pages == None:
    glv.args.verbose = 1
    glv.devmsg("Specify either --all or --pages to set which pages to generate.")
    exit(0)

  main()