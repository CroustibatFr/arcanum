import glv
import wiki_renderer
from arcanum_data import ArcanumData

class Class(ArcanumData):
  """ The Class class """
  
  def __init__(self, json_data):
    super().__init__(json_data)
  
  @staticmethod
  def category_page():
    """ Generate -/wikis/Classes """
    output = \
      "|Name|Requirements|Costs|Benefits|Unlocks|Tier|\n" \
      "|:---|:-----------|:---:|:-------|:------|:---|\n"
    sortl = []
    sortl.append((-2, "apprentice"))
    for class_id in glv.data.all_data["classes"]:
      if glv.data.all_data["classes"][class_id].tags == "t_job":
        sortl.append((-1, class_id))
      elif glv.data.all_data["classes"][class_id].tags is not None:
        # To be clear, this grabs the last character of the tag, which should the the number of the tier
        # and converts it to an int.
        sortl.append((int(glv.data.all_data["classes"][class_id].tags[-1]), class_id))
    glv.devmsg("Before sorting:\n{0}".format(sortl)) if glv.args.verbose > 1 else None
    sortl.sort()
    glv.devmsg("After sorting:\n{0}".format(sortl)) if glv.args.verbose > 1 else None
    sortl = [v[1] for v in sortl]
    glv.devmsg("After stripping sorting values.\n{0}".format(sortl)) if glv.args.verbose > 1 else None
    tmp = []
    for class_id in sortl:
      klass = glv.data.all_data["classes"][class_id]
      tmp.append(klass.category_line() + "\n")
    output += "".join(tmp)
    glv.devmsg(output) if glv.args.verbose > 2 else None
    title = "Classes"
    path = "home/"
    diff = wiki_renderer.save_page(title, path, output)
    glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
    glv.wikipost(title, path, output, diff)
  
  def category_line(self):
    """
    Transform this Class object into a line of Markdown text for the Classes page
    :return: string of gitlab markdown text
    """
    symbol = self.get_symbol()
    title = f"[{glv.data.id_slug[self.id][1]}]({glv.data.id_slug[self.id][0]})"
    reqs = self.get_requirements()
    costs = glv.data.id_list_to_wiki(self.get_run_costs(), style="single")
    benefits = glv.data.id_list_to_wiki(self.get_all_rewards(), style="single")
    unlocks = glv.data.id_list_to_wiki(self.get_unlocks(), style="single")
    tier = self.tags
    return f"|{title}<br>{symbol}|{reqs}|{costs}|{benefits}|{unlocks}|{tier}|"

  @classmethod
  def entry_pages(cls):
    """
    Generate a page for each Class
    :return: None
    """
    for class_key in glv.data.all_data["classes"].keys():
      klass = glv.data.all_data["classes"][class_key]
      title = glv.data.id_slug[class_key][1]
      # Get requirement chain in mermaid format
      markup = ""
      if glv.args.graph:
        markup += klass.get_requirement_chain()
      markup += wiki_renderer.entry_page(klass)
      path = "home/Classes/"
      diff = wiki_renderer.save_page(title, path, markup)
      glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
      glv.wikipost(title, path, markup, diff)
  
  def get_requirement_chain(self):
    """
    Get all prerequisites for this class
    :return Mermaid markup string
    """
    # devmsg(self.id)
    if self.require:
      requires = self.require.replace("g.tier0>0&&", "").replace("g.tier1>0&&", "")\
        .replace("g.tier2>0&&", "").replace("g.tier3>0&&", "")\
        .replace("g.tier4>0&&", "").replace("g.tier5>0&&", "")\
        .replace("&&g.tier1==0", "").replace("&&g.tier2==0", "")\
        .replace("&&g.tier3==0", "").replace("&&g.tier4==0", "")\
        .replace("&&g.tier5==0", "").replace("&&g.tier6==0", "")\
        .replace("&&g.evil==0", " and not Evil")
      # devmsg(f"Requires: {requires}")
      requires = requires.replace(">=", " ≥ ").replace("<=", " ≤ ")\
        .replace("g.", "").replace("&&", " and ").replace("||", " or ") \
        .replace(">", " > ").replace("+", " + ")\
        .replace("(", " ( ").replace(")", " ) ")
      # devmsg(f"Requires: {requires}")
    else:
      requires = ""
    # Split, resolve, and join requirements
    items = []
    for item in requires.split():
      obj = glv.data.get_object(item)
      if obj is not None:
        items.append(obj.get_title())
      else:
        items.append(item)
    # devmsg(f"items: {items}")
    requires = " ".join(items)
    requires = requires.replace(" and ", ",<br>")\
      .replace("( ", "(").replace(" )", ")")
    
    # devmsg(f"Requires: {requires}")
    
    tier = 0
    if self.tags is not None:
      # devmsg(f"Tags: {self.tags}")
      if "t_tier" in self.tags:
        tier = int(self.tags.replace("t_tier", ""))
    
    mermaid = ""
    if tier > 0:
      prev = tier - 1
      mermaid = f"```mermaid\n"
      mermaid += f"graph LR\n"
      mermaid += f"  t{prev}{{Tier {prev} Class}}\n"
      mermaid += f"  t{prev}r([\"{requires}\"])\n"
      mermaid += f"  t{prev} ---> t{prev}r ---> {self.id}\n"
      mermaid += f"  subgraph tier{tier} [Tier {tier}]\n"
      mermaid += f"  {self.id}([{self.get_title()}])\n"
      mermaid += f"  end\n"
      mermaid += f"```\n"
    
    # devmsg(f"mermaid:\n{mermaid}")
    return mermaid
  
  @staticmethod
  def get_classes_for_tier(tier):
    """
    Get the classes for integer tier input
    :param tier: integer tier number
    :return: array of class ids
    """
    class_ids = []
    tier_tag = f"tier{tier}"
    for class_id in glv.data.all_data["classes"].keys():
      tags = glv.data.all_data["classes"][class_id].tags
      if tags and tier_tag in tags:
        class_ids.append(class_id)
    return class_ids

