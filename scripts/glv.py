import requests
import os
import sys
import traceback
import urllib.parse
from datetime import datetime
from os import path

mains = None
args = None
data = None

def devmsg(msg):
  if args.verbose > 0:
    ts = datetime.now().time()
    c = sys._getframe().f_back.f_code.co_name
    f = path.basename(sys._getframe().f_back.f_code.co_filename)
    l = sys._getframe().f_back.f_lineno
    spc = ' ' * sys._getframe().f_back.f_code.co_stacksize
    if msg == 'trace':
        print(f'{ts}{spc}{f} {c} {l}: tracing...')
        traceback.print_stack()
        return
    # print(f'{ts}{spc}{f} {c} {l}: {msg}')
    print(f'{ts} {f} {c} ({l}): {msg}')
    return

def wikipost(title, path, content, diff):
  if not args.upload:
    return
  if args.diff and not diff:
    return
  
  if args.test:
    path = "test/" + path
  base_url = "https://gitlab.com/api/v4/projects/22897802/wikis/"
  headers = {"PRIVATE-TOKEN": os.getenv("ARCANUM_WIKI_KEY")}
  urlencoded_slug = urllib.parse.quote(path + title, safe='')
  target = base_url + urlencoded_slug

  devmsg(f"Attempting to upload: {target}") if args.verbose > 0 else None
  
  # Check if slug already exists
  # devmsg(f"Checking if '{slug}' already exists..")
  r = requests.get(target)
  # print(f"r:({r}) text:({r.text})")
  # devmsg(f"status code is {r.status_code}...")

  # rate limiting
  if r.status_code == 429:
    devmsg("Status Code 429")
    devmsg(r)
    exit(1)

  if r.status_code == 404:
    devmsg("Status Code 404, not found. Attempting to create page.") if args.verbose > 0 else None
    devmsg(f"Response: {r} {r.text}")
    # devmsg(f"'{slug}' doesn't exist, creating..")
    # Create a new page with a slug title, so slug generates properly
    payload = {"format": "markdown", "title": path + title, "content": "tbd"}
    r = requests.post(base_url, headers=headers, data=payload)
    # print(f"Create results: [{r}] ({r.text})")
    if r.status_code != 201:
      devmsg("Page not created")
      devmsg(f"base_url({base_url} target({path + title})")
      devmsg(f"Response: {r} {r.text}")
      devmsg(f"content: {content}")
      exit(1)
    devmsg("Page created.") if args.verbose > 0 else None

  devmsg("Page exists. Attempting to update.") if args.verbose > 0 else None
  # Overwrite content (possibly new tbd content) with proper content
  # devmsg(f"'{slug}' exists, updating...")
  payload = {
    "format": "markdown",
    "title": path + title,
    "content": content
  }
  r = requests.put(target, headers=headers, data=payload)
  if r.status_code != 200:
    #devmsg(f"slug({slug}) title({title})")
    devmsg(f"Response: {r} {r.text}")
    #devmsg(f"content: {content}")
    exit(1)
  devmsg("Upload success.") if args.verbose > 0 else None

def gen_slug_title(path, id):
  override = {
    "enc_chest1": "Dusty Chest (enc_chest1)",
    "enc_chest3": "Dusty Chest (enc_chest3)",
    "enc_w_faz_nothingthere": "Behind You (enc_w_faz_nothingthere)",
    "enc_w_faz_somethingthere": "Behind You (enc_w_faz_somethingthere)"
  }
  if id in override:
    return (path + override[id].replace(" ", "-").replace("?", "_"), override[id])
  full_slug = path + default_slug(id)
  title = data.get_object(id).get_title()
  return (full_slug, title)

def default_slug(id):
  return data.get_object(id).get_title().replace(" ", "-").replace("?", "_")
