import glv
from arcanum_data import ArcanumData

class Task(ArcanumData):
  """ The Task class """
  def __init__(self, json_data):
    super().__init__(json_data)

  def get_requires(self):
    """
    Get the things this local needs to be unlocked
    :return: list of IDs
    """
    req_list = []
    import re
    if self.require is not None:
      if type(self.require) == list:
        req_list.append(self.require)
      else:
        # glv.devmsg(f"require: {self.require}")
        # glv.devmsg(f"type: {type(self.require)}")
        # glv.devmsg(f"task: {vars(self)}")
        res = re.split('&|>=|<=|>|<|=|\+|\|', self.require)
        for re in res:
          re = re.replace("(", "").replace(")", "")
          if re == "":
            continue
          if re.isnumeric():
            continue
          if re.startswith("g."):
            re = re.replace("g.", "")
          req_list.append(re)
    if self.need is not None:
      if type(self.need) == list:
        req_list.append(self.need)
      else:
        need = self.need.replace("(", "").replace(")", "").replace(">=", " ")\
          .replace(">", " ").replace("||", " ").replace("<=", " ").replace("<", " ")

        needs = need.split()
        for n in needs:
          if n == "":
            continue
          if n.isnumeric():
            continue
          if n.startswith("g."):
            n = n.replace("g.", "")
          req_list.append(n)
    # glv.devmsg(f"require list: {req_list}")
    return req_list
