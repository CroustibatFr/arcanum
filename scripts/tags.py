import glv
import wiki_renderer

class Tags:
  """ Store tags here in this class """
  storage = dict()
  
  def __init__(self):
    pass
  
  @classmethod
  def add(cls, tag, object_id):
    if glv.data.all_data["tags"] is None:
      glv.data.all_data["tags"] = Tags()
    if tag in cls.storage:
      if object_id in cls.storage[tag]:
        devmsg(f"'{object_id}' already in tag '{tag}'")
      else:
        cls.storage[tag].append(object_id)
    else:
      cls.storage[tag] = [object_id]
      title = cls.get_title(tag)
      slug = "home/Tags"
      glv.data.id_slug[tag] = (slug, title)
    return

  @classmethod
  def category_page(cls):
    """ Tags page """

    output = \
      "|Tag Name|Tagged Items|\n" \
      "|:-------|:-----------|\n"
    sorted_keys = list(cls.storage.keys())
    sorted_keys.sort()
    item_list = []
    for key in sorted_keys:
      line = f"|{key}|"
      items = cls.storage[key]
      items.sort()
      line += glv.data.id_list_to_wiki(items)
      line += "|"
      item_list.append(line)
      
    output += "<br>\n".join(item_list)
    title = "Tags"
    path = "home/"
    diff = wiki_renderer.save_page(title, path, output)
    glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
    glv.wikipost(title, path, output, diff)
  
  @classmethod
  def entry_pages(cls):
    """
    Generate a page for each Locale
    :return: None
    """
    for key in glv.data.all_data["tags"].keys():
      obj = glv.data.all_data["tags"][key]
      title = glv.data.id_slug[key][1]
      markup = wiki_renderer.entry_page(obj)
      path = "home/Potions/"
      diff = wiki_renderer.save_page(title, path, markup)
      glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
      if glv.args.upload and not glv.args.test and (not glv.args.diff or diff): 
        glv.wikipost(title, path, markup)
    
  def get_title(tag):
    """ Returns the title of the object """
    title = tag
    title = title.title().replace("'S", "'s").replace(" Of ", " of ")\
      .replace(" The ", " the ").replace(" Iv", " IV").replace(" Iii", " III")\
      .replace(" Ii", " II")
    return title
