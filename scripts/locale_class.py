import glv
import wiki_renderer
from arcanum_data import ArcanumData

class Locale(ArcanumData):
  """  The Locale Class """
  def __init__(self, json_data):
    super().__init__(json_data)
  
  def get_requires(self):
    """
    Get the things this locale needs to be unlocked
    :return: list of IDs
    """
    req_list = []
    import re
    if self.require is not None:
      res = re.split('&|>=|<=|>|<|=|\+', self.require)
      for re in res:
        re = re.replace("(", "").replace(")", "")
        if re == "":
          continue
        if re.isnumeric():
          continue
        if re.startswith("g."):
          re = re.replace("g.", "")
        req_list.append(re)
    # glv.devmsg(f"require list: {req_list}")
    return req_list
  
  @staticmethod
  def category_page():
    """ Generate -/wikis/Locales """
    output = \
      "|Name|Length|Dist|Level|Required|Reward|Skill Exp|Max Increase|Unlocks|\n" \
      "|:---|:----:|:--:|:---:|:-------|:-----|:--------|:-----------|:------|\n"
    tmp = []
    for locale_id in glv.data.all_data["locales"].keys():
      if locale_id == "loc_ettinmoors":
        continue  # skip this blank one
      locale = glv.data.all_data["locales"][locale_id]
      tmp.append(locale.category_line() + "\n")
    tmp.sort()
    output += "".join(tmp)
    title = "Locales"
    path = "home/"
    diff = wiki_renderer.save_page(title, path, output)
    glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
    glv.wikipost(title, path, output, diff)
  
  def category_line(self):
    """
    Transform this Locale object into a line of Markdown text for the Locales page
    :return: string of gitlab markdown text
    """
    symbol = self.get_symbol()
    title = f"[{glv.data.id_slug[self.id][1]}]({glv.data.id_slug[self.id][0]})"
    length = self.get_length()
    dist = self.get_dist()
    level = self.get_level()
    required = self.get_requirements()
    if required == "":
      required = f"[Level](level) ≥ {level - 1}"
    rewards = glv.data.id_list_to_wiki(self.get_resource_rewards())
    exps = glv.data.id_list_to_wiki(self.get_exp_rewards())
    maxs = glv.data.id_list_to_wiki(self.get_max_rewards())
    unlocks = glv.data.id_list_to_wiki(self.get_unlocks())
    return f"|{title}<br>{symbol}|{length}|{dist}|{level}|{required}|{rewards}|{exps}|{maxs}|{unlocks}|"

  @classmethod
  def entry_pages(cls):
    """
    Generate a page for each Locale
    :return: None
    """
    for locale_id in glv.data.all_data["locales"].keys():
      obj = glv.data.all_data["locales"][locale_id]
      title = glv.data.id_slug[locale_id][1]
      # devmsg(f"Locale: {locale_id} ({title})")
      markup = wiki_renderer.entry_page(obj)
      path = "home/Locales/"
      diff = wiki_renderer.save_page(title, path, markup)
      glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
      glv.wikipost(title, path, markup, diff)

  def get_symbol(self):
    """ Return the emoji symbol for this object, if any """
    if self.sym is not None:
      symbol = self.sym
    else:  # Defaults to tree
      symbol = "🌳"
    return "<span style=\"font-size:2em;\">" + symbol + "</span>"
