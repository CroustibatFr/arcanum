import glv
import wiki_renderer
from arcanum_data import ArcanumData

class Home(ArcanumData):
  """ The Home class """
  def __init__(self, json_data):
    super().__init__(json_data)
  
  @staticmethod
  def category_page():
    """ Generate -/wikis/Homes """
    output = \
      "|Name|Size|Tags|Cost|Effects|Requirements|\n" \
      "|:---|:--:|:---|:---|:------|:-----------|\n"
    tmp = []
    for home_id in glv.data.all_data["homes"].keys():
      home = glv.data.all_data["homes"][home_id]
      tmp.append(home.category_line() + "\n")
    tmp.sort()
    output += "".join(tmp)
    title = "Homes"
    path = "home/"
    diff = wiki_renderer.save_page(title, path, output)
    glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
    glv.wikipost(title, path, output, diff)

  def category_line(self):
    """
    Transform this Home object into a line of Markdown text for the Homes page
    :return: string of gitlab markdown text
    """
    symbol = self.get_symbol()
    title = f"[{glv.data.id_slug[self.id][1]}]({glv.data.id_slug[self.id][0]})"
    desc = self.desc
    if desc is not None:
      desc = f"<br>*{desc}*"
    size = self.mod["space.max"]
    tags = self.tags
    if tags is None:
      tags = ""
    else:
      if "," in tags:
        temp_tags = []
        for t in tags.split(','):
          if t.startswith("t_"):
            t = t.replace("t_", "")
          temp_tags.append(t)
        temp_tags.sort()
        tags = ", ".join(temp_tags)
      else:
        if tags.startswith("t_"):
          tags = tags.replace("t_", "")
      tags = f"*{tags}*"
    cost = glv.data.id_list_to_wiki(self.get_costs(), style="single")
    effect = glv.data.id_list_to_wiki(self.get_mods(), style="single")
    required = self.get_requirements()
    return f"|{title}{symbol}{desc}|{size}|{tags}|{cost}|{effect}|{required}|"

  def get_costs(self):
    """
    Get the costs of buying this Home
    :return: list of IDs
    """
    cost_list = []
    if self.cost is not None:
      if type(self.cost) == dict:
        for key, val in self.cost.items():
          cost_list.append([str(val), key])
      elif type(self.cost) == int:
        cost_list.append([str(self.cost), "[Gold](gold)"])
      else:
        glv.devmsg(f"Unknown type '{type(self.cost)}' for Home cost: {self.cost}")
        exit(1)
    # print(f"run cost fo
    return cost_list
