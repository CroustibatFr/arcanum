import glv
from arcanum_data import ArcanumData

class Weapon(ArcanumData):
  """ The Weapon class """
  def __init__(self, json_data):
    super().__init__(json_data)
