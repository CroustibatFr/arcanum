import glv
import wiki_renderer
from arcanum_data import ArcanumData

class Player(ArcanumData):
  """ The Player class """
  def __init__(self, json_data):
    super().__init__(json_data)

  @staticmethod
  def category_page():
    """ Generate -/wikis/Player """
    output = "This page is a default list of the items in this category.<br>\n"
    tmp = []
    for obj in glv.data.all_data["player"].keys():
      item = glv.data.all_data["player"][obj]
      tmp.append(f"[{glv.data.id_slug[obj][1]}]({glv.data.id_slug[obj][0]})")
    tmp.sort()
    output += "<br>\n".join(tmp)
    title = "Player"
    path = "home/"
    diff = wiki_renderer.save_page(title, path, output)
    glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
    glv.wikipost(title, path, output, diff)
  
  @classmethod
  def entry_pages(cls):
    """
    Generate a page for each stat
    :return: None
    """
    for key in glv.data.all_data["player"].keys():
      obj = glv.data.all_data["player"][key]
      title = glv.data.id_slug[key][1]
      markup = wiki_renderer.entry_page(obj)
      path = "home/Player/"
      diff = wiki_renderer.save_page(title, path, markup)
      glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
      glv.wikipost(title, path, markup, diff)
