import glv
import wiki_renderer
from arcanum_data import ArcanumData

class Dungeon(ArcanumData):
  """ The Dungeon class """
  def __init__(self, json_data):
    super().__init__(json_data)

  @staticmethod
  def category_page():
    """ Generate -/wikis/Dungeons """
    output = \
      "|Name|Level|Length|Distance|Requirement|Consumption|Reward|Encounters|Boss|\n" \
      "|:---|:---:|:----:|:------:|:----------|:----------|:-----|:---------|:---|\n"
    tmp = []
    for dungeon_id in glv.data.all_data["dungeons"].keys():
      dungeon = glv.data.all_data["dungeons"][dungeon_id]
      tmp.append(dungeon.category_line() + "\n")
    tmp.sort()
    output += "".join(tmp)
    title = "Dungeons"
    path = "home/"
    diff = wiki_renderer.save_page(title, path, output)
    glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
    glv.wikipost(title, path, output, diff)
    
  def category_line(self):
    """
    Transform this Dungeon object into a line of Markdown text
    :return: string of gitlab markdown text
    """
    symbol = self.get_symbol()
    title = f"[{glv.data.id_slug[self.id][1]}]({glv.data.id_slug[self.id][0]})"
    level = self.get_level()
    length = self.get_length()
    dist = self.get_dist()
    requirements = self.get_requirements()
    costs = glv.data.id_list_to_wiki(self.get_run_costs())
    rewards = glv.data.id_list_to_wiki(self.get_resource_rewards())
    encounters = glv.data.id_list_to_wiki(self.get_encounters())
    bosses = glv.data.id_list_to_wiki(self.get_bosses())
    return f"|{title}<br>{symbol}|{level}|{length}|{dist}|{requirements}|{costs}|{rewards}|{encounters}|{bosses}|"

  def get_symbol(self):
    """ Return the emoji symbol for this object, if any """
    if self.sym is not None:
      symbol = self.sym
    else:  # Defaults to crossed swords
      symbol = "⚔"
    return "<span style=\"font-size:2em;\">" + symbol + "</span>"
