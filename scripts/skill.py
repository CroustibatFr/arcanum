import glv
from arcanum_data import ArcanumData

class Skill(ArcanumData):
  """ The Skill class """
  def __init__(self, json_data):
    super().__init__(json_data)

  def get_requires(self):
    """
    Get the things this local needs to be unlocked
    :return: list of IDs
    """
    req_list = []
    import re
    if self.require is not None:
      res = re.split('&|>=|<=|>|<|=|\+', self.require)
      for re in res:
        re = re.replace("(", "").replace(")", "")
        if re == "":
          continue
        if re.isnumeric():
          continue
        if re.startswith("g."):
          re = re.replace("g.", "")
        req_list.append(re)
    # glv.devmsg(f"require list: {req_list}")
    return req_list
