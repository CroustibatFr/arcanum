import glv
import wiki_renderer
from arcanum_data import ArcanumData

class Monster(ArcanumData):
  """ The Monster class """
  def __init__(self, json_data):
    super().__init__(json_data)

  @staticmethod
  def category_page():
    """ Generate -/wikis/Monsters """
    nm = "Name"
    lv = "Level"
    hp = "HP"
    db = "Defense<br>Bonus"
    re = "Regen"
    th = "To Hit<br>Bonus"
    sb = "Speed<br>Bonus"
    un = "Unique"
    at = "Attack"
    output = \
      f"|{nm}|{lv}|{hp}|{db}|{re}|{th}|{sb}|{un}|{at}|\n" \
      f"|:---|:--:|:--:|:--:|:--:|:--:|:--:|:--:|:--:|\n"
    tmp = []
    for monster_id in glv.data.all_data["monsters"].keys():
      monster = glv.data.all_data["monsters"][monster_id]
      tmp.append(monster.category_line() + "\n")
    tmp.sort()
    output += "".join(tmp)
    title = "Monsters"
    path = "home/"
    diff = wiki_renderer.save_page(title, path, output)
    glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
    glv.wikipost(title, path, output, diff)

  def category_line(self):
    """
    Transform this Monster object into a line of Markdown text for the Locales page
    :return: string of gitlab markdown text
    """
    sym = self.get_symbol()
    title = f"[{glv.data.id_slug[self.id][1]}]({glv.data.id_slug[self.id][0]})"
    nm = f"{sym} {title}"
    if self.desc is not None and self.desc != "":
      nm += f"<br>*{self.desc}*"
    lv = self.get_level()
    hp = self.hp
    df = self.defense
    re = self.regen
    th = self.tohit
    sb = self.speed
    un = "Yes" if self.unique else "No"
    at = "; ".join(self.get_attacks())
    return f"|{nm}|{lv}|{hp}|{df}|{re}|{th}|{sb}|{un}|{at}|"
  
  def get_attacks(self):
    """
    Format this monster's attack data
    :return: list of markdown strings
    """
    attacks = []
    if self.attack is None:
      return attacks
    # devmsg(f"{self.id} attack: {self.attack}")
    name = None
    kind = None
    if "dot" in self.attack:
      dot = self.attack["dot"]
      if "name" in dot and "kind" in dot and "dmg" in dot and "duration" in dot:
        nm = dot["name"].title()
        ki = dot["kind"]
        dm = dot["dmg"]
        du = dot["duration"]
        attacks.append(f"{nm}: {dm} {ki} damage for {du} seconds")
      elif "name" in dot:
        name = dot["name"]
      elif "adj" in dot:
        name = dot["adj"]
      if name is not None:
        attacks.append(f"Dot: {name}")
      
    if "name" in self.attack:
      name = self.attack["name"].title()
      dmg = None
      if "dmg" in self.attack:
        dmg = self.attack["dmg"]
      elif "dot" in self.attack and "dmg" in self.attack["dot"]:
        dmg = self.attack["dot"]["dmg"]
      if "kind" in self.attack:
        kind = self.attack["kind"]
      if kind is None and "type" in self.attack:
        kind = self.attack["type"]
      attacks.append(f"{name}: {dmg} {kind} damage")
    return attacks
