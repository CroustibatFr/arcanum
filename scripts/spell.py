import glv
from arcanum_data import ArcanumData

class Spell(ArcanumData):
  """ The Spell class """
  def __init__(self, json_data):
    super().__init__(json_data)
