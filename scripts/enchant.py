import glv
import wiki_renderer
from arcanum_data import ArcanumData

class Enchant(ArcanumData):
  """ The Enchant class """
  def __init__(self, json_data):
    super().__init__(json_data)

  @staticmethod
  def category_page():
    """ Generate -/wikis/Enchants """
    nm = "Name"
    rq = "Required"
    bu = "Buy Cost"
    le = "Level"
    sl = "Slot"
    ef = "Effect"
    cc = "Cast Cost"
    output = \
      f"|{nm}|{rq}|{bu}|{le}|{sl}|{ef}|{cc}|\n" \
      f"|:---|:---|:---|:--:|:---|:---|:---|\n"
    tmp = []
    for enchant_id in glv.data.all_data["enchants"].keys():
      enchant = glv.data.all_data["enchants"][enchant_id]
      tmp.append(enchant.category_line() + "\n")
    tmp.sort()
    output += "".join(tmp)
    title = "Enchants"
    path = "home/"
    diff = wiki_renderer.save_page(title, path, output)
    glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
    glv.wikipost(title, path, output, diff)

  def category_line(self):
    """
    Transform this Enchant object into a line of Markdown text
    :return: string of gitlab markdown text
    """
    symbol = self.get_symbol()
    title = f"[{glv.data.id_slug[self.id][1]}]({glv.data.id_slug[self.id][0]})"
    if self.desc is not None and self.desc != "":
      title += f"<br>*{self.desc}*"
    req = self.get_requirements()
    lev = self.get_level()
    buy = glv.data.id_list_to_wiki(self.get_buy_costs(), style="single")
    slt = self.get_slots()
    efc = self.get_alters()
    cst = glv.data.id_list_to_wiki(self.get_run_costs(), style="single")
    return f"|{symbol} {title}|{req}|{buy}|{lev}|{slt}|{efc}|{cst}|"
  
  def get_alters(self):
    """
    Return a nice markup text for alteration effects
    :return: GitLab markdown text
    """
    alters = []
    for key, val in self.alter.items():
      # devmsg(f"({self.id}) {key}, {val}")
      if key.startswith("mod.player.resist."):
        subkey = key.replace("mod.player.resist.", "").title()
        key = subkey + " Resist"
      elif key == "attack.bonus":  key = "Attack Bonus"
      elif key == "attack.damage": key = "Attack Damage"
      elif key == "attack.kind":   key = "Attack"
      elif key == "attack.tohit":  key = "To Hit"
      elif key == "mod.lifedrain.attack.dot.damage.min":
        key = "[Lifedrain](lifedrain) Min Damage"
      elif key == "mod.lifedrain.attack.dot.damage.max":
        key = "[Lifedrain](lifedrain) Max Damage"
      elif key == "mod.blast.attack.dot.damage.min":
        key = "[Blast](blast) Min Damage"
      elif key == "mod.blast.attack.dot.damage.max":
        key = "[Blast](blast) Max Damage"
      elif key == "mod.trapsoul.result.souls.max":
        key = "[Trap Soul](trapsoul) Max Souls"
      elif key == "attack.action.targets":
        if val == "ally":
          key = "Targets Allies"
          val = ""
        else:
          key = "Attack Target"
      elif key == "attack.dot.targets":
        key = "Targets Allies"
        val = ""
      elif key == "attack.dot.mod.dmg.min":
        key = "Dot Min Damage"
      elif key == "attack.dot.mod.dmg.max":
        key = "Dot Max Damage"
      elif key == "attack.action.result.hp.min":
        key = "HP Minimum"
      elif key == "attack.action.result.hp.max":
        key = "HP Maximum"
      elif key == "element.rate":
        key = "Elemental Mana Rate"
      elif key == "mod.prismatic.rate":
        key = "Prismatic Mana Rate"
      elif key == "mod.stress.rate":
        key = "Stress Rate"
      elif key == "mod.stress.max":
        key = "Stress Max"
      elif key == "mod.focus.cost.mana":
        key = "[Focus](focus) Mana Cost"
      elif key == "mod.focus.result.runner.exp":
        key = "[Focus](focus) Exp"
      elif key == "mod.player.bonuses.mana":
        key = "Bonus Mana"
      elif key == "mod.spelllist.max":
        key = "Max Spell List"
      elif key.startswith("mod."):
        key = key.replace("mod.", "")
      name = glv.data.id_list_to_wiki([key])
      indicator = "+"
      # devmsg(f"val type is {type(val)}")
      if (type(val) == int or type(val) == float) and val < 0:
        indicator = ""
      elif val == "":
        indicator = ""
      elif type(val) == str:
        if not val.endswith("%"):
          val = glv.data.id_list_to_wiki([val])
      alters.append(f"{indicator}{val} {name}")
    return ",<br>".join(alters)
    
  def get_slots(self):
    """ Return a nice markup text for slots """
    s = self.only
    slots = []
    if "," in s:
      for i in s.split(","):
        slots.append(self.get_slot_name(i))
    else:
      slots.append(self.get_slot_name(s))
    # devmsg(f"slots: {slots}")
    return ",<br>".join(slots)
  
  @staticmethod
  def get_slot_name(item):
    """
    Given a slot name, find a good title for it
    :return: string of normal titlecase text
    """
    if item in glv.data.all_data["weapons"]:
      return glv.data.all_data["weapons"][item].get_title()
    else:
      # devmsg(f"no title for '{item}'")
      return item.title()
