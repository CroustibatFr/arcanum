import os
import glv
  
def category_page(generic_data):
  """ Default untemplated category page """
  output = "This page is a default list of the items in this category.<br>\n"
  output += "If it doesn't meet your needs, please let a developer "
  output += "know, and they'll get around to updating it.<br>\n\n"
  domain = generic_data.__name__.lower()
  if domain == "class":
    domain = "classes"
  elif domain == "player":
    pass
  else:
    domain += "s"
  title = domain.title()
  glv.devmsg(f"Generating generic page for '{title}'")
  domain_data = getattr(Data, domain)
  item_list = []
  for key in domain_data.keys():
    item = domain_data[key]
    item_list.append(item.to_slug())
  item_list.sort()
  output += "<br>\n".join(item_list)
  # glv.devmsg(output)
  
def entry_page(generic_data):
  """ Default untemplated entry page """
  key_to_title = {
    "actname": "Action Name",
    "actdesc": "Action Description",
    "alias": "Alias",
    "buy": "Cost to Buy",
    "cost": "Cost to acquire",
    "desc": "Description",
    "disable": "Class disables",
    "dist": "Distance",
    "effect": "Effects",
    "encs": "Encounters",
    "flavor": "Flavor",
    "hide": "Hidden",
    "id": "ID",
    "length": "Length",
    "level": "Level",
    "locked": "Locked",
    "log": "Log Entry",
    "loot": "Loot",
    "max": "Maximum",
    "mod": "Modifiers",
    "name": "Name",
    "need": "Need",
    "rate": "Rate",
    "require": "Requirements",
    "result": "Result",
    "run": "Run Cost",
    "secret": "Secret",
    "sell": "Gains from selling",
    "stack": "Stackable",
    "start": "Start Announcement",
    "stat": "Statistics",
    "sym": "Symbol",
    "tags": "Tags",
    "title": "Title",
    "unit": "Only Integer Values",
    "use": "Effect of using",
    "val": "Value",
    "warn": "Pop-up Warning",
  }
  resolve_list = [
    "buy", "cost", "disable", "effect", "encs", "log", "loot", "mod",
    "need", "require", "result", "run", "start", "tags", "use",
  ]
  output = ""
  for key in vars(generic_data):
    if key in ["id", "name", "buyname"]:
      continue  # Skip these fields
    title = key_to_title[key]
    val = getattr(generic_data, key)
    t = type(val)
    if key == "require":
      output += f"**{title}**: " + generic_data.get_requirements() + "\n\n"
      continue
    if key not in resolve_list:
      if key == "name":
        val = generic_data.get_title()
      if val != "" or val == {}:
        output += f"**{title}**: {val}\n\n"
    elif t == dict:
      items = []
      output += f"**{title}**\n\n"
      for key2, val2 in val.items():
        thing = glv.data.id_list_to_wiki([key2])
        output += f"- {thing}: {val2}\n\n"
    elif t == str:
      thing = glv.data.id_list_to_wiki([val])
      output += f"**{title}**: {thing}\n\n"
    elif t == list:
      if not val:  # skip "disable": [],
        continue
      output += f"**{title}**\n\n"
      for item in val:
        thing = glv.data.id_list_to_wiki([item])
        output += f"- {thing}\n\n"
    else:
      devmsg(f"key({key}) val({val}) invalid val type ({type(val)})")
      raise Exception(f"Unknown value type: {type(val)}")
  
  # output += "\n---\n*This page was automatically generated from a default template.*\n"
  return output
    
def get_title_with_suffix(title, suffix=[]):
  """ Returns the title of the object with prefix and suffixes."""
  texts = [title]
  if suffix:
    texts.extend(suffix)
  text = " ".join(texts)
  return text

def default_slug(id):
  item = glv.data.get_object(id)
  slug = item.get_title().replace(" ", "-").replace("?", "_")
  return slug

def get_markdown_link(text, slug, prefix=[]):
  prefixes = ""
  if prefix:  # quantity really
    for p in prefix:
      prefixes += f": {p}"
  output = f"[{text}]({slug}){prefixes}"
  # glv.devmsg(output)
  return output

def save_page(title, path, content):
  cmp_filename = path + title + ".md"
  write_filename = cmp_filename
  if glv.args.test:
    write_filename = "test/" + write_filename
  if not os.path.exists(cmp_filename) or content != open(cmp_filename).read():
    os.makedirs(os.path.dirname(write_filename), exist_ok=True)
    with open(write_filename, "w", encoding="UTF-8") as page:
      glv.devmsg(f"Writing {write_filename}")
      page.write(content)
    return True
  return False
