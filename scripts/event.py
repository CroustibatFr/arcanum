import glv
from arcanum_data import ArcanumData

class Event(ArcanumData):
    """ The Event class """
    def __init__(self, json_data):
        super().__init__(json_data)
    
    def to_slug(self, prefix=None, suffix=None):
        """
        Generates and returns the Event's page link
        This overrides the parent class's to_slug(),
        so consider yourself warned.
        :param prefix: list of prefixes
        :param suffix: list of suffixes
        :type prefix: list
        :type suffix: list
        """
        title = self.get_title()
        # glv.devmsg(f"Event {self.id} slug: prefix({prefix}) suffix({suffix})")
        if self.id.startswith("tier"):
            tier = self.id.replace("tier", "")
            return f"[Tier {tier}]({self.id})"
        
        if prefix:
            prefixes = " ".join(prefix)
            output = f"Event: [{title}]({self.id}): {prefixes}"
        else:
            output = f"Event: [{title}]({self.id})"
        return output
