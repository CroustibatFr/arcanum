import glv
import wiki_renderer
from arcanum_data import ArcanumData

class Encounter(ArcanumData):
  """ The Encounter class """
  def __init__(self, json_data):
    super().__init__(json_data)

  @staticmethod
  def category_page():
    """ Generate -/wikis/Encounters """
    output = \
      "|Name|Effects|Stresses|Rewards|\n" \
      "|:---|:------|:-------|:------|\n"
    tmp = []
    for encounter_id in glv.data.all_data["encounters"].keys():
      encounter = glv.data.all_data["encounters"][encounter_id]
      tmp.append(encounter.category_line() + "\n")
    tmp.sort()
    output += "".join(tmp)
    title = "Encounters"
    path = "home/"
    diff = wiki_renderer.save_page(title, path, output)
    glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
    glv.wikipost(title, path, output, diff)

  def category_line(self):
    """
    Transform this Encounter object into a line of Markdown text
    :return: string of gitlab markdown text
    """
    symbol = self.get_symbol()
    title = f"[{glv.data.id_slug[self.id][1]}]({glv.data.id_slug[self.id][0]})"
    if self.desc is not None and self.desc != "":
      title += f"<br>*{self.desc}*"
    # effects = glv.data.id_list_to_wiki(self.get_effects())
    # rewards = glv.data.id_list_to_wiki(self.get_resource_rewards())
    e, s, r = self.get_things()
    effects = glv.data.id_list_to_wiki(e, style="single")
    stresses = glv.data.id_list_to_wiki(s, style="single")
    rewards = glv.data.id_list_to_wiki(r, style="single")
    return f"|{symbol} {title}|{effects}|{stresses}|{rewards}|"
  
  def get_things(self):
    """
    Build lists of effects, stresses, and rewards for this Encounter
    :return: array of 3 arrays
    """
    effects = []
    stresses = []
    rewards = []
    if self.effect is not None:
      for key, val in self.effect.items():
        if key == "wear":  # typo
          key = "weary"
        # glv.devmsg(f"key({key}) val({val})")
        if key in glv.data.all_data["stressors"]:
          stresses.append([val, key])
        elif key in glv.data.all_data["resources"]:
          effects.append([val, key])
        elif key in glv.data.all_data["player"]:
          effects.append([val, key])
        elif key.startswith("player."):
          key = key.replace("player.", "")
          if key in glv.data.all_data["player"]:
            effects.append([val, key])
          else:
            glv.devmsg(F"key 'player {key}' not found in player")
            exit(1)
        elif key.endswith(".exp"):
          key2 = key.replace(".exp", "")
          if key2 in glv.data.all_data["skills"]:
            effects.append([val, key])
          elif "s_" + key2 in glv.data.all_data["skills"]:
            effects.append([val, key])
          else:
            glv.devmsg(f"exp key '{key2}' not found in skills")
            exit(1)
        elif key.endswith(".rate"):
          key = key.replace(".rate", "")
          if key in glv.data.all_data["skills"]:
            effects.append([val, key])
          else:
            glv.devmsg(f"rate key '{key}' not found in skills")
            exit(1)
        elif key.endswith(".max"):
          key = key.replace(".max", "")
          if key in glv.data.all_data["skills"]:
            effects.append([val, key])
          elif key in glv.data.all_data["resources"]:
            effects.append([val, key])
          else:
            glv.devmsg(f"max key '{key}' not found in skills/resource")
            exit(1)
        elif key == "stress":
          # TODO: 'stress' is a tag, fix plz
          stresses.append([val, key])
        else:
          glv.devmsg(f"I don't know about effect '{key}'")
          exit(1)
    
    if self.result is not None:
      for key, val in self.result.items():
        # glv.devmsg(f"{self.id} results key({key}) val({val})")
        if key in glv.data.all_data["resources"]:
          rewards.append([val, key])
        elif key in glv.data.all_data["events"]:
          rewards.append([val, key])
        elif key.endswith(".max"):
          key2 = key.replace(".max", "")
          if key2 in glv.data.all_data["skills"]:
            rewards.append([val, key])
          elif key2 in glv.data.all_data["resources"]:
            rewards.append([val, key])
          else:
            glv.devmsg(f"max key '{key}' not found in skills/resource")
            exit(1)
        elif key.endswith(".rate"):
          key = key.replace(".rate", "")
          if key in glv.data.all_data["skills"]:
            rewards.append([val, key])
          elif key in glv.data.all_data["resources"]:
            rewards.append([val, key])
          else:
            glv.devmsg(f"rate key '{key}' not found in skills")
            exit(1)
        elif key.endswith(".exp"):
          key2 = key.replace(".exp", "")
          if key2 in glv.data.all_data["skills"]:
            rewards.append([val, key])
          elif "s_" + key2 in glv.data.all_data["skills"]:
            rewards.append([val, key])
          else:
            glv.devmsg(f"exp key '{key2}' not found in skills")
            exit(1)
        else:
          glv.devmsg(f"result key '{key}' not found in resources")
          print(self)
          exit(1)
    
    return effects, stresses, rewards

  @classmethod
  def entry_pages(cls):
    """
    Generate a page for each Encounter
    :return: None
    """
    for key in glv.data.all_data["encounters"].keys():
      obj = glv.data.all_data["encounters"][key]
      title = glv.data.id_slug[key][1]
      markup = wiki_renderer.entry_page(obj)
      path = "home/Encounters/"
      diff = wiki_renderer.save_page(title, path, markup)
      glv.devmsg(f"File {path + title} diff is {diff}.") if glv.args.verbose > 0 and glv.args.diff else None
      glv.wikipost(title, path, markup, diff)
