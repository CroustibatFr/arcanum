import math
import glv
from tags import Tags

class ArcanumData:
  """ Base class for the data objects """
  # Default all of the valid attributes in all of the data
  action = None
  actdesc = None
  actname = None
  adj = None
  alias = None
  alter = None
  armor = None
  at = None
  attack = None
  bars = None
  biome = None
  bonus = None
  boss = None
  buy = None
  buyname = None
  cd = None
  choice = None
  color = None
  cost = None
  damage = None
  defense = None
  desc = None
  disable = None
  dist = None
  dodge = None
  dot = None
  effect = None
  enchants = None
  encs = None
  enemies = None
  every = None
  evil = None
  evilamt = None
  exclude = None
  fill = None
  flavor = None
  group = None
  hands = None
  hide = None
  hp = None
  id = None
  immune = None
  kind = None
  length = None
  level = None
  lock = None
  locked = None
  log = None
  loot = None
  material = None
  max = None
  mod = None
  name = None
  need = None
  noproc = None
  only = None
  owned = None
  perpetual = None
  priceMod = None
  properties = None
  rate = None
  reflect = None
  regen = None
  repeat = None
  require = None
  resist = None
  result = None
  reverse = None
  run = None
  runmod = None
  save = None
  scale = None
  school = None
  secret = None
  sell = None
  silent = None
  slot = None
  spawns = None
  speed = None
  spells = None
  stack = None
  start = None
  stat = None
  sym = None
  tags = None
  title = None
  tohit = None
  type = None
  unique = None
  unit = None
  unused = None
  use = None
  val = None
  verb = None
  warn = None
  weight = None

  def __init__(self, json_data):
    for key, val in json_data.items():
      if key == "distance":  # TODO: combat tutorial goof
        key = "dist"
      if hasattr(self, key):
        setattr(self, key, val)
      else:
        glv.devmsg(f"{type(self)} has no key '{key}'")
        exit(1)
      if key == "tags":
        if "," in val:
          tag_list = val.split(",")
          for tag in tag_list:
            Tags.add(tag, self.id)
        else:
          Tags.add(val, self.id)

  def get_all_rewards(self):
    """
    Get this object's skill experience rewards
    :return: list of skill IDs
    """
    tmp = dict()
    if self.result is not None:
      for i in self.result:
        tmp[i] = True
    if self.effect is not None:
      for i in self.effect:
        tmp[i] = True
    if self.loot is not None:
      for i in self.loot:
        tmp[i] = True
    if self.mod is not None:
      if type(self.mod) == str:
        tmp[self.mod] = True
      else:
        for i in self.mod:
          tmp[i] = True
    if self.encs is not None:
      for i in self.encs:
        items = glv.data.all_data["encounters"][i].get_all_rewards()
        for item in items:
          tmp[item] = True
    if self.spawns is not None:
      for i in self.spawns:
        if type(i) == dict:
          i = i["ids"]  # fix some weird jazid thing
        if type(i) == list:
          for i2 in i:
            items = glv.data.all_data["monsters"][i2].get_all_rewards()
            for item in items:
              tmp[item] = True
        else:
          if i == "lgbird":
            continue  # doesn't exist
          if i == "level" or i == "range":
            continue  # non-trackable format: "spawns":{"level":"15~23","range":2}
          items = glv.data.all_data["monsters"][i].get_all_rewards()
          for item in items:
            tmp[item] = True
    all_list = list(tmp.keys())
    all_list.sort()
    return all_list

  def get_bosses(self):
    """
    Get a list of all bosses for this object
    :return: list of boss IDs
    """
    tmp = dict()
    # glv.devmsg(f"getting bosses for {self.id}")
    if self.boss is not None:
      boss = self.boss
      if type(boss) == str:
        boss = [boss]
      elif type(boss) == dict:
        boss = list(boss.values())
      for i in boss:
        # glv.devmsg(f"boss is {i}")
        if type(i) == list:
          for i2 in i:
            tmp[i2] = True
        else:
          tmp[i] = True
    boss_list = list(tmp.keys())
    boss_list.sort()
    return boss_list

  def get_buy_costs(self):
    """
    Get the costs of buying this object
    :return: list of IDs
    """
    cost_list = []
    if self.buy is not None:
      for key, val in self.buy.items():
        cost_list.append([str(val), key])
    # glv.devmsg(f"run cost for {self.id}: {cost_list}")
    return cost_list
  
  def get_dist(self):
    """ Returns the distance to a locale or dungeon """
    if self.dist is not None:
      return self.dist
    dist = 0
    try:
      dist = math.ceil(4.4 * math.exp(0.30 * self.get_level()))
    except TypeError as e:
      glv.devmsg(f"Error: {e}")
      glv.devmsg(vars(self))
      exit(1)
    return dist

  def get_effects(self):
    """
    Get the effects of running this object
    :return: list of [value, ID]
    """
    effect_list = []
    if self.effect is not None:
      for key, val in self.effect.items():
        effect_list.append([str(val), key])
    return effect_list

  def get_encounters(self):
    """
    Get a list of all encounters for this object
    :return: list of encounter IDs
    """
    tmp = dict()
    if self.spawns is not None:
      for i in self.spawns:
        if type(i) == dict:
          i = i["ids"]  # fix some weird jazid thing
        if type(i) == list:
          for i2 in i:
            tmp[i2] = True
        else:
          if i == "lgbird":
            continue  # doesn't exist
          if i == "level" or i == "range":
            continue  # untrackable format: "spawns":{"level":"15~23","range":2}
          tmp[i] = True
    enc_list = list(tmp.keys())
    enc_list.sort()
    return enc_list

  def get_exp_rewards(self):
    """
    Get this object's skill experience rewards
    :return: list of skill IDs
    """
    tmp = dict()
    if self.result is not None:
      for i in self.result:
        if i == "player.exp":
          continue  # player exp
        if i.endswith(".exp"):
          key = i.replace(".exp", "")
          tmp[key] = True
    if self.effect is not None:
      for i in self.effect:
        if i == "player.exp":
          continue  # player exp
        if i.endswith(".exp"):
          key = i.replace(".exp", "")
          tmp[key] = True
    if self.loot is not None:
      for i in self.loot:
        if i.endswith(".exp"):
          key = i.replace(".exp", "")
          tmp[key] = True
    if self.mod is not None:
      for i in self.mod:
        if i.endswith(".exp"):
          key = i.replace(".exp", "")
          tmp[key] = True
    if self.encs is not None:
      for i in self.encs:
        if i == "movingsnow":
          # This doesn't really exist
          continue
        items = glv.data.all_data["encounters"][i].get_exp_rewards()
        for item in items:
          tmp[item] = True
    if self.spawns is not None:
      for i in self.spawns:
        if type(i) == dict:
          i = i["ids"]  # fix some weird jazid thing
        if type(i) == list:
          for i2 in i:
            items = glv.data.all_data["monsters"][i2].get_exp_rewards()
            for item in items:
              tmp[item] = True
        else:
          if i == "lgbird":
            continue  # doesn't exist
          if i == "level" or i == "range":
            continue  # non-trackable format: "spawns":{"level":"15~23","range":2}
          items = glv.data.all_data["monsters"][i].get_exp_rewards()
          for item in items:
            tmp[item] = True
    exp_list = list(tmp.keys())
    exp_list.sort()
    return exp_list
  
  def get_length(self):
    """ Returns the number of iterations a locale or dungeon has """
    if self.length is not None:
      return self.length
    else:
      return 5 * self.get_level()

  def get_level(self):
    if self.level is not None:
      return self.level
    else:
      return 1

  def get_max_rewards(self):
    """
    Get this Locale's skill max rewards
    :return: list of skill IDs
    """
    tmp = {}
    if self.result is not None:
      for i in self.result:
        if i.endswith(".max"):
          key = i.replace(".max", "")
          tmp[key] = True
    if self.loot is not None:
      for i in self.loot:
        if i.endswith(".max"):
          key = i.replace(".max", "")
          tmp[key] = True
    if self.mod is not None:
      for i in self.mod:
        if i.endswith(".max"):
          key = i.replace(".max", "")
          tmp[key] = True
    if self.effect is not None:
      for i in self.effect:
        if i.endswith(".max"):
          key = i.replace(".max", "")
          tmp[key] = True
    if self.encs is not None:
      for i in self.encs:
        if i == "movingsnow":
          # This doesn't really exist
          continue
        items = glv.data.all_data["encounters"][i].get_max_rewards()
        for item in items:
          tmp[item] = True
    if self.spawns is not None:
      for i in self.spawns:
        if type(i) == dict:
          i = i["ids"]  # fix some weird jazid thing
        if type(i) == list:
          for i2 in i:
            items = glv.data.all_data["monsters"][i2].get_max_rewards()
            for item in items:
              tmp[item] = True
        else:
          if i == "lgbird":
            continue  # doesn't exist
          if i == "level" or i == "range":
            continue  # untrackable format: "spawns":{"level":"15~23","range":2}
          items = glv.data.all_data["monsters"][i].get_max_rewards()
          for item in items:
            tmp[item] = True
  
    max_list = list(tmp.keys())
    max_list.sort()
    return max_list

  def get_mods(self):
    """
    Get the mods of running this object
    :return: list of [value, ID]
    """
    mod_list = []
    if self.mod is not None:
      for key, val in self.mod.items():
        val = str(val)
        if key == "rest.effect.stamina":
          mod_list.append([val, "[Rest Stamina](rest)"])
        elif key == "t_runes.max":
          mod_list.append([val, "[Runestone Max](runestones)"])
        elif key == "notoriety.rate":
          mod_list.append([val, "[Notoriety Rate](fame)"])
        elif key == "codices.mod.research.max":
          mod_list.append([val, "[Codices Research Max](codices)"])
        elif key == "tomes.mod.research.max":
          mod_list.append([val, "[Tomes Research Max](tomes)"])
        elif key == "tomes.mod.arcana.max":
          mod_list.append([val, "[Tomes Arcana Max](tomes)"])
        elif key == "study.effect.research":
          mod_list.append([val, "[Studying Research](study)"])
        elif key == "assemblepuppet.cost.gold":
          mod_list.append([val, "[Puppet Gold Cost](assemblepuppet)"])
        elif key == "assemblepuppet.cost.research":
          mod_list.append([val, "[Puppet Research Cost](assemblepuppet)"])
        elif key == "assemblepuppet.result.puppets":
          mod_list.append([val, "[Puppet Quantity Bonus](assemblepuppet)"])
        elif key == "assembleautomata.cost.gold":
          mod_list.append([val, "[Automata Gold Cost](assembleautomata)"])
        elif key == "assembleautomata.cost.research":
          mod_list.append([val, "[Automata Research Cost](assembleautomata)"])
        elif key == "assembleautomata.cost.runestones":
          mod_list.append([val, "[Automata Runestone Cost](assembleautomata)"])
        elif key == "assemblemachina.cost.gold":
          mod_list.append([val, "[Machina Gold Cost](machinaassembly)"])
        elif key == "assemblemachina.cost.research":
          mod_list.append([val, "[Machina Research Cost](machinaassembly)"])
        elif key == "assemblemachina.result.machinae":
          mod_list.append([val, "[Machina Quantity Bonus](machinaassembly)"])
        elif key == "iceflame.attack.dot.duration":
          mod_list.append([val, "[Iceflame Dot Duration](iceflame)"])
        elif key == "livingsnow.mod.livingsnow.rate":
          mod_list.append([val, "[Livingsnow Rate](livingsnow)"])
        elif key == "hearth.cost.firegem":
          mod_list.append([val, "[Hearth Firegem Cost](hearth)"])
        elif key == "liquifier.mod.managem.rate":
          mod_list.append([val, "[Liquifier Managem Rate](liquifier)"])
        elif key == "machinaautomation.mod.space":
          mod_list.append([val, "[Machina Automation Space](machina)"])
        elif key == "machinagarage.mod.space":
          mod_list.append([val, "[Machina Garage Space](machina)"])
        elif key == "puppetautomation.mod.space":
          mod_list.append([val, "[Puppet Automation Space](puppets)"])
        elif key == "puppetworkshop.mod.space":
          mod_list.append([val, "[Puppet Workshop Space](puppets)"])
        elif key == "shapinglab.mod.space":
          mod_list.append([val, "[Shaping Lab Space](shapinglab)"])
        else:
          mod_list.append([str(val), key])
    return mod_list
  
  def get_requirements(self):
    """
    Get the things required to be able to do this object
    :return: string of GitLab markdown text
    """
    elements = []
    if self.require is None:
      return ""
    elif type(self.require) == str:
      raw_line = self.require.replace(">=", " ≥ ").replace("<=", " ≤ ")\
        .replace(">", " > ").replace("<", " < ").replace("+", " + ")\
        .replace("==", " = ").replace("(", "( ").replace(")", " )")\
        .replace("&&", " and ").replace("||", " or ")
      for term in raw_line.split():
        # devmsg(f"attempting to resolve {self.id} term '{term}'...")
        entity = term  # default if not resolvable
        if term.startswith("g."):
          subterm = term.replace("g.", "")
          # devmsg(f"attempting to resolve subterm '{subterm}'...")
          if subterm.startswith("player."):
            subsubterm = subterm.replace("player.", "")
            slug = glv.data.id_list_to_wiki([subsubterm])
          elif subterm.endswith(".value"):  # g.apprentice.value
            subsubterm = subterm.replace(".value", "")
            slug = glv.data.id_list_to_wiki([subsubterm])
          else:
            slug = glv.data.id_list_to_wiki([subterm])
          # devmsg(f"slug for {subterm} is {slug}")
          entity = slug
        elif term.startswith("evt_"):
          entity = glv.data.id_list_to_wiki([term])
        elif term in glv.data.all_data["events"]:
          entity = glv.data.id_list_to_wiki([term])
        elif term in glv.data.all_data["resources"]:
          entity = glv.data.id_list_to_wiki([term])
        elif term in glv.data.all_data["skills"]:
          entity = glv.data.id_list_to_wiki([term])
        elif term in glv.data.all_data["tasks"]:
          entity = glv.data.id_list_to_wiki([term])
        elif term in glv.data.all_data["locales"]:
          entity = glv.data.id_list_to_wiki([term])
        elif term in glv.data.all_data["dungeons"]:
          entity = glv.data.id_list_to_wiki([term])
        elif term in glv.data.all_data["classes"]:
          entity = glv.data.id_list_to_wiki([term])
        elif term in glv.data.all_data["upgrades"]:
          entity = glv.data.id_list_to_wiki([term])
        elif term.startswith("tier"):
          entity = glv.data.id_list_to_wiki([term])
        elif term == "mustylibrary":
          entity = glv.data.id_list_to_wiki([term])
        elif term in ["≥", "≤", "<", ">", "=", "+", "and", "or", "+", "(", ")"]:
          pass
        elif term.isnumeric():
          pass
        else:
          devmsg(f"{self.id} term '{term}' not resolved!")
          exit(1)
        elements.append(entity)
      # devmsg(elements)
      output = " ".join(elements).replace("( ", "(").replace(" )", ")")
      # devmsg(f"output: {output}")
      return output
    elif type(self.require) == list:
      # devmsg(f"{self.id} require is a list")
      for i in self.require:
        elements.append(glv.data.id_list_to_wiki([i]))
      output = ",<br>".join(elements)
      # devmsg(f"output: {output}")
      return output
    else:
      devmsg(f"{self.id} require unknown: {self.require}")
      exit(1)

  def get_resource_rewards(self):
    """
    Get this object's resource rewards
    :return: list of resource IDs
    """
    tmp = dict()
    if self.result is not None:  # {'arcana': 0.1, 'research': 10}
      for i in self.result:
        if i in glv.data.all_data["resources"]:
          tmp[i] = True
        elif i in glv.data.all_data["rares"]:
          tmp[i] = True
    if self.loot is not None:
      for i in self.loot:
        if i in glv.data.all_data["resources"]:
          tmp[i] = True
        elif i in glv.data.all_data["rares"]:
          tmp[i] = True
    if self.encs is not None:
      for i in self.encs:
        if i == "movingsnow":
          # This doesn't really exist
          continue
        items = glv.data.all_data["encounters"][i].get_resource_rewards()
        for item in items:
          tmp[item] = True
    if self.spawns is not None:
      for i in self.spawns:
        if type(i) == dict:
          i = i["ids"]  # fix some weird jazid thing
        if type(i) == list:
          for i2 in i:
            items = glv.data.all_data["monsters"][i2].get_resource_rewards()
            for item in items:
              tmp[item] = True
        else:
          if i == "lgbird":
            continue  # doesn't exist
          if i == "level" or i == "range":
            continue  # untrackable format: "spawns":{"level":"15~23","range":2}
          if i in glv.data.all_data["monsters"]:
            items = glv.data.all_data["monsters"][i].get_resource_rewards()
            for item in items:
              tmp[item] = True
          else:
            glv.devmsg(f"item '{i}' not in glv.data.all_data[\"monsters\"]!")
            exit(1)
    resource_list = list(tmp.keys())
    resource_list.sort()
    return resource_list

  def get_run_costs(self):
    """
    Get the costs of running this object
    :return: list of IDs
    """
    cost_list = []
    if self.run is not None:
      for key, val in self.run.items():
        cost_list.append([str(val), key])
    if self.cost is not None:
      for key, val in self.cost.items():
        cost_list.append([str(val), key])
    # glv.devmsg(f"run cost for {self.id}: {cost_list}")
    return cost_list

  def get_symbol(self):
    """ Return the emoji symbol for this object, if any """
    if self.sym is not None:
      symbol = self.sym
    else:
      return ""
    return "<span style=\"font-size:2em;\">" + symbol + "</span>"
    
  def get_title(self):
    """ Returns the title of the object """
    title = self.id
    if self.name is not None:
      title = self.name
    title = title.title().replace("'S", "'s").replace(" Of ", " of ")\
      .replace(" The ", " the ").replace(" Iv", " IV").replace(" Iii", " III")\
      .replace(" Ii", " II")
    return title

  def get_unlocks(self):
    """
    Get a list of things which require this object to unlock
    :return: list of IDs
    """
    tmp = dict()
    # Check unlocks
    # TODO: other types perhaps?
    for item_id in glv.data.all_data["locales"]:
      item = glv.data.all_data["locales"][item_id]
      if self.id in item.get_requires():
        tmp[item_id] = True
    for item_id in glv.data.all_data["skills"]:
      item = glv.data.all_data["skills"][item_id]
      if self.id in item.get_requires():
        tmp[item_id] = True
    for item_id in glv.data.all_data["tasks"]:
      item = glv.data.all_data["tasks"][item_id]
      if self.id in item.get_requires():
        tmp[item_id] = True
    unlock_list = list(tmp.keys())
    unlock_list.sort()
    return unlock_list

  def __str__(self):
    field_list = []
    members = [attr for attr in dir(ArcanumData) if not callable(getattr(ArcanumData, attr)) and not attr.startswith("__")]
    for field in members:
      if getattr(self, field) is not None:
        field_list.append("    {0}: {1}".format(field, getattr(self, field)))
    return "\n".join(field_list)
