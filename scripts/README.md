This directory contains the code that generates the gitlab wiki.

To upload pages you will need to [generate a personal access token](https://gitlab.com/-/profile/personal_access_tokens) for the script to use for uploading webpages and assign it to a environment variable ARCANUM_WIKI_KEY for access.

To run the script run main.py. There are a variety of arguments that the script can be run with. Those arguments can be seen with the help flag --help, or by reading the code in main.py.

Good luck. This thing is a nightmare.